//
//  Fowlboard.swift
//  TastyImitationKeyboard
//
//  Created by Tim Eichholz on 2/16/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

let kHeight = "kHeight"

class UserLexicon
{
  private var nextID = 0
  private var idToWord: [String] = []
  
}

class Lexicon
{
  var corrections: [String:String] = [:]
  
  init(_ lexicon: UILexicon)
  {
    for entry in lexicon.entries
    {
      corrections[entry.userInput] = entry.documentText
    }
  }
  
  init()
  {
  }
  
  subscript(_ input: String) -> String?
  {
    return corrections[input]
  }
}

class AutoCorrections: ExtraView
{
  var correctionsView: UIStackView
  var keyboard: Fowlboard? = nil
  
  required init(globalColors: GlobalColors.Type?, darkMode: Bool, solidColorMode: Bool) {
    correctionsView = UIStackView()
    super.init(globalColors: globalColors, darkMode: darkMode, solidColorMode: solidColorMode)
    addSubview(correctionsView)
    
    correctionsView.alignment = .leading
    correctionsView.axis = .horizontal
    correctionsView.distribution = .equalSpacing // .fillProportionally
//    correctionsView.clipsToBounds = true
    
//    var L: UILabel = label("Hello")
//    L.backgroundColor = UIColor.red.withAlphaComponent(0.2)
//    correctionsView.addArrangedSubview(L)
//    L = label("foo")
//    L.backgroundColor = UIColor.green.withAlphaComponent(0.2)
//    correctionsView.addArrangedSubview(L)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  convenience init(globalColors: GlobalColors.Type?, darkMode: Bool, solidColorMode: Bool, keyboard: Fowlboard?) {
    self.init(globalColors: globalColors, darkMode: darkMode, solidColorMode: solidColorMode)
    self.keyboard = keyboard
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    correctionsView.center = center
    correctionsView.bounds = bounds
  }
  
  func label(_ string: String) -> UIView
  {
    let x = UIButton()
    x.setTitle(string, for: UIControlState.normal)
    x.addTarget(self, action: #selector(wordChosen2(_:)), for: .touchUpInside)
//    let tap = UITapGestureRecognizer(target: self, action: #selector(wordChosen(_:)))
//    tap.numberOfTapsRequired = 1
//    x.addGestureRecognizer(tap)
//    x.isUserInteractionEnabled = true
    return x
  }
  
  func wordChosen2(_ x: UIButton)
  {
    clearSuggestions()
    chooseWord( (x.titleLabel?.text)! )
  }
  
  func wordChosen(_ rec: UITapGestureRecognizer)
  {
    if let v = rec.view as? UILabel
    {
      chooseWord(v.text!)
    }
  }
  
//  var keyboard: Fowlboard?
  func chooseWord(_ word: String)
  {
    if let kb = keyboard
    {
      kb.chooseWord(word)
    }
  }
  
  func clearSuggestions()
  {
    correctionsView.subviews.forEach({ $0.removeFromSuperview() })
  }
  func suggest(_ string: String)
  {
    correctionsView.addArrangedSubview(label(string))
  }
}

class Fowlboard: KeyboardViewController
{
  var lexicon: Lexicon?

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//    UserDefaults.standard.register(defaults: [kHeight: 1.0/3.0])
    requestSupplementaryLexicon(completion: {[unowned self] lex in
      self.lexicon = Lexicon(lex)
    })
  }
  
  required init?(coder: NSCoder)
  {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func createBanner() -> ExtraView? {
    return AutoCorrections(globalColors: type(of: self).globalColors, darkMode: darkMode(), solidColorMode: solidColorMode(), keyboard: self)
  }
  
  
  enum CorrectionState {
    case notTyping
    case typing
    case postCorrection
    case correctionCancelled
  }
  var correctionState = CorrectionState.typing
  
  override func backspaceDown(_ sender: KeyboardKey)
  {
    if correctionState == .postCorrection
    {
      // undo autocorrect, move to .correctionCancelled
    }
    else if correctionState == .correctionCancelled
    {
      // delete two characters, change to .typing
    }
    else
    {
      super.backspaceDown(sender)
      clearSuggestions()
      if let cs = checkForCorrections()
      {
        suggest(cs)
      }
    }
  }
  
  func checkWord() -> (Range<String.Index>, String)?
  {
    if let str = textDocumentProxy.documentContextBeforeInput
    {
      var wordStart = str.endIndex
      for chr in str.characters.lazy.reversed()
      {
        if chr == " " { break }
        wordStart = str.index(before: wordStart)
      }
      if wordStart != str.endIndex
      {
        let range: Range<String.Index> = wordStart ..< str.endIndex
        return (range, str)
      }
    }
    return nil
  }
  
  func chooseWord(_ word: String)
  {
    if let (r,s) = checkWord()
    {
      //erase current word and insert new word
      var length = s.distance(from: r.lowerBound, to: r.upperBound)
      while length > 0
      {
        textDocumentProxy.deleteBackward()
        length -= 1
      }
      textDocumentProxy.insertText(word)
    }
  }
  
  func lastWord() -> String?
  {
    if let (r, s) = checkWord()
    {
      return s[r]
    }
    return nil
  }
  
  func completions() -> [String]?
  {
    if let word = lastWord()
    {
      return checker.completions(forPartialWordRange: NSMakeRange(0, word.characters.count), in: word, language: "en-US")
    }
    return nil
  }
  
  var checker = UITextChecker()
  
  func checkForCorrections() -> [String]?
  {
    if let (range,str) = checkWord()
    {
      let str = str[range]
      let nsrange = NSRange(location: 0, length: str.utf16.count)
//      NSMakeRange(
//        str.distance(from: str.startIndex, to: range.lowerBound),
//        str.distance(from: range.lowerBound, to: range.upperBound) )
      var result: [String]?
      result = [str]
      if let res = checker.completions(forPartialWordRange: nsrange, in: str, language: "en-US")
      {
        result = (result ?? []) + res
      }
      if let res = checker.guesses(forWordRange: nsrange, in: str, language: "en-US")
      {
        result = (result ?? []) + res
      }
      return result
    }
    
    return nil
  }
  
  func clearSuggestions()
  {
    if let v = self.bannerView as? AutoCorrections
    {
      v.clearSuggestions()
    }
  }
  func suggest(_ words: [String])
  {
    if let v = self.bannerView as? AutoCorrections
    {
      for i in 0 ..< 3
      {
        if i >= words.count { break }
        v.suggest(words[i])
      }
    }
  }
  
  func applyCorrection() -> Bool
  {
  
    if let v = self.bannerView as? AutoCorrections
    {
    
//      v.correctionsView.subviews.forEach({ $0.removeFromSuperview() })
      
      let p = { [unowned v] (str: String?) -> Void in
        let s = str != nil ? ("\"" + str! + "\"") : "n/a"
        v.correctionsView.addArrangedSubview(v.label(s))
      }
      

      if let words = checkForCorrections()
      {
        chooseWord(words.first!)
        clearSuggestions()
//        clearSuggestions()
//        let ws = words[ 0 ... 3 ]
//        suggest([ String(words.count)])
//        suggest(Array(ws)) // words.startIndex ... words.startIndex.advanced(by: 3) ])
//        p( String(words.count) )
        
//        for w in words { p(w) }
      }
      
      
//      p( textDocumentProxy.documentContextBeforeInput )  // ?? "n/a")
//      p( textDocumentProxy.documentContextAfterInput  ) // ?? "n/a" )
    
//      if let str = textDocumentProxy.documentContextBeforeInput
//      {
//        var wordStart = str.endIndex // characters.count
//        for chr in str.characters.lazy.reversed()
//        {
//          if chr == " " { break }
//          wordStart = str.index(before: wordStart)
//        }
//        if wordStart != str.endIndex
//        {
//          let word = str.substring(from: wordStart)
//          p(word)
//        }
//      }
//    }
//    return false;
    }
    return false;
  }
  
  override func keyPressedHelper(_ sender: KeyboardKey)
  {
    if let model = self.layout?.keyForView(sender)
    {
      switch model.type
      {
      case .space:
        if correctionState == .typing
        {
          // apply autocorrect, change to .postCorrection
          // or don't apply autocorrect, add the last word to the dictionary, change to .notTyping
          if(applyCorrection())
          {
            correctionState = .postCorrection
          }
          else
          {
            correctionState = .notTyping
          }
        }
      case .character:
        super.keyPressedHelper(sender)
        if correctionState == .notTyping { correctionState = .typing }
        if let cs = completions()
        {
          clearSuggestions()
          if let res = checkWord()
          {
            let w = res.1[res.0]
            suggest([ w])
          }
          suggest(cs)
        }
        else
        {
          suggest([ "n/a"])
        }
        return
      case .specialCharacter:
        let s = model.outputForCase(false)
        switch(s)
        {
        case ",","."," ":
          if correctionState == .typing
          {
            // apply autocorrect
            let _ = applyCorrection()
          }
        default:
          // other special character or .character
          
          break;
        }
      default: break
      }
      
    }
    
    super.keyPressedHelper(sender)
  }
  
  
}


